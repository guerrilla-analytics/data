import os
import csv

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
DATA_LOG_FILE = "../datalog.csv"

if __name__ == '__main__':
    # Remove unnecessary files (only needed to allow for empty directories at Git origin)
    os.remove(os.path.join(PROJECT_DIRECTORY, "supporting/.gitkeep"))
    # Add to datalog.csv if requested (presumes finalized rows)
    if '{{ cookiecutter.add_to_datalog }}'.lower() in ('y', 'yes'):
        with open(DATA_LOG_FILE, 'a') as csv_file:
            datalog_writer = csv.writer(csv_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            datalog_writer.writerow(['{{cookiecutter.data_uid}}', '{{cookiecutter.filename}}',
                                     '{{cookiecutter.description}}', '{{cookiecutter.received_from}}',
                                     '{{cookiecutter.received_by}}', '{{cookiecutter.received_on}}'])
